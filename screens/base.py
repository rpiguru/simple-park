import os
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.screenmanager import Screen
from widgets.dialog import LoadingDialog


Builder.load_file(os.path.join(os.path.dirname(__file__), 'base.kv'))


class BaseScreen(Screen):
    app = ObjectProperty()
    loading_dlg = None

    def __init__(self, **kwargs):
        super(BaseScreen, self).__init__(**kwargs)
        self.app = App.get_running_app()
        self.loading_dlg = LoadingDialog()

    def switch_screen(self, screen_name, direction=None):
        self.app.switch_screen(screen_name=screen_name, direction=direction)

    def on_touch_down(self, touch):
        if self.app.clk_back_light:
            super(BaseScreen, self).on_touch_down(touch)
        self.app.on_touched()
