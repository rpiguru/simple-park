# -*- coding: iso8859-15 -*-
"""
Lock Up: Send 3V3 constant output to relay.
Unlock Up: Stop 3V3 constant output to relay.
Lock Down: Ignore input form physical button.
Unlock down: Wait for physical button input.(Normal Mode)
Raise: Send 3V3 output to relay for 2 sec
"""
import os
import time
from functools import partial
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import DictProperty

from kivymd.snackbar import Snackbar
from screens.home.tabs.base import BaseTab
from settings import MQTT_TOPIC_ENTRY_RAISE, MQTT_TOPIC_EXIT_RAISE, MQTT_TOPIC_ENTRY_LOCK_UP, MQTT_TOPIC_EXIT_LOCK_UP, \
    MQTT_TOPIC_ENTRY_UNLOCK_UP, MQTT_TOPIC_EXIT_UNLOCK_UP
from utils.common import logger
from utils.db import lock_gate
from utils.web import get_locked_state

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'controls.kv'))


class ControlsTab(BaseTab):

    index = 2

    last_click_time = {'entry': 0, 'exit': 0}

    locked = DictProperty()

    def on_enter_tab(self, *args):
        for pos in {'entry', 'exit'}:
            self.locked[pos] = get_locked_state(pos)
            if self.locked[pos]:
                self.ids['btn_down_{}'.format(pos)].source = 'assets/images/controls/tickets_locked.png'
            if self.get_app().locked_up_state.get(pos):
                self.ids['btn_up_{}'.format(pos)].source = 'assets/images/controls/barrier_up_lock.png'

    def on_btn_down_entry(self, btn):
        val = True if 'open' in btn.source else False
        if val:
            _replace_image(btn, 'assets/images/controls/tickets_locked.png')
        else:
            _replace_image(btn, 'assets/images/controls/tickets_open.png')
        lock_gate('entry', val)
        self.locked['entry'] = val

    def on_btn_down_exit(self, btn):
        val = True if 'unlock' in btn.source else False
        if val:
            _replace_image(btn, 'assets/images/controls/lock_exit_gate_down.png')
        else:
            _replace_image(btn, 'assets/images/controls/unlock_exit_gate_down.png')
        lock_gate('exit', val)
        self.locked['exit'] = val

    def on_btn_raise(self, btn, pos):
        if self.locked.get(pos):
            Snackbar(text='{} is locked!'.format(pos.capitalize()), background_color=(.8, 0, .3, .5)).show()
            return

        if time.time() - self.last_click_time[pos] > 5:
            self.last_click_time[pos] = time.time().__int__()
            self.get_app().publish_mqtt_message(
                topic=MQTT_TOPIC_ENTRY_RAISE if pos == 'entry' else MQTT_TOPIC_EXIT_RAISE,
                payload='START')
            Snackbar(text='Raising {} Gate Arm...'.format(pos.capitalize()), duration=1).show()
            _replace_image(btn, 'assets/images/controls/barrier_up.png')
            Clock.schedule_once(partial(_replace_image, btn, 'assets/images/controls/barrier_closed.png'), 5)
        else:
            logger.warning('`Raise` button is pressed, but cannot execute in 5 sec!')

    def on_btn_up(self, btn, pos):
        val = True if 'unlock' in btn.source else False
        if val:
            _replace_image(btn, 'assets/images/controls/barrier_up_lock.png')
            topic = MQTT_TOPIC_ENTRY_LOCK_UP if pos == 'entry' else MQTT_TOPIC_EXIT_LOCK_UP
        else:
            _replace_image(btn, 'assets/images/controls/barrier_down_unlock.png')
            topic = MQTT_TOPIC_ENTRY_UNLOCK_UP if pos == 'entry' else MQTT_TOPIC_EXIT_UNLOCK_UP
        self.get_app().locked_up_state[pos] = val
        self.get_app().publish_mqtt_message(topic=topic, payload='START')


def _replace_image(widget, path, *args):
    anim = Animation(opacity=0, duration=.3, t='in_sine')
    anim.bind(on_complete=partial(_on_first_animation, path))
    anim.start(widget)


def _on_first_animation(new_path, *args):
    widget = args[1]
    widget.source = new_path
    anim = Animation(opacity=1, duration=.3, t='in_sine')
    anim.start(widget)
