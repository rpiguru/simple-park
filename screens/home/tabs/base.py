# -*- coding: iso8859-15 -*-
import datetime
import os
from abc import abstractmethod
from kivy.app import App
from kivy.lang import Builder
from kivy.properties import ObjectProperty, NumericProperty
from kivy.clock import Clock
from kivy.uix.screenmanager import Screen
from kivy.uix.label import Label

from widgets.datetime.calendar.calendar_ui import DatePicker
from widgets.datetime.time_picker import TimePicker

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'base.kv'))


class BaseTab(Screen):

    screen_widget = ObjectProperty(None)
    index = NumericProperty(0)

    def on_enter(self, *args):
        Clock.schedule_once(self.on_enter_tab, .1)
        super(BaseTab, self).on_enter(*args)

    @abstractmethod
    def on_enter_tab(self, *args):
        """
        Inherit this function and customize this if you need to do something when entering to tab...
        DO NOT use `on_enter` function directly in each tab!!!
        """
        pass

    @staticmethod
    def get_app():
        return App.get_running_app()

    def _switch_screen(self, screen_name, direction=None, *args):
        self.get_app().switch_screen(screen_name, direction)


class DatetimeMixin(object):

    date_picker = None
    time_picker = None

    def __init__(self, **kwargs):
        super(DatetimeMixin, self).__init__(**kwargs)

    def on_enter_tab(self, *args):
        Clock.schedule_once(lambda dt: self.add_pickers())
        Clock.schedule_interval(lambda dt: self.update_time(), 60)

    def add_pickers(self):
        self.date_picker = DatePicker(size_hint=(None, None), width=120, height=40)
        self.time_picker = TimePicker(size_hint=(None, None), width=65, height=40)
        self.ids.container.add_widget(self.date_picker)
        self.ids.container.add_widget(Label(text="_", size_hint=(None, None), size=(20, 30), pos_hint={'center_y': .45},
                                            color=(0, 0, 0, 1)))
        self.ids.container.add_widget(self.time_picker)
        self.update_time()

    def update_time(self):
        self.time_picker.set_value(datetime.datetime.now().strftime("%H:%M"))
