# -*- coding: iso8859-15 -*-
import datetime
import os
import threading
from functools import partial
from kivy.clock import Clock, mainthread
from kivy.lang import Builder
from kivymd.snackbar import Snackbar
from screens.home.tabs.base import BaseTab, DatetimeMixin
from utils.web import extend_code


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'extend_ticket.kv'))


class ExtendTicketTab(DatetimeMixin, BaseTab):

    index = 1

    def on_btn_submit(self):
        code = self.ids.code.text
        if len(code) != 4:
            Snackbar(text='Invalid Code Value!', background_color=(.8, 0, .3, .5)).show()
            return

        self.screen_widget.loading_dlg.open()
        code = int(code)
        dt_str = self.date_picker.text + ' ' + self.time_picker.text
        dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")
        threading.Thread(target=self.send_extend_request, args=(code, dt)).start()

    def send_extend_request(self, code, dt):
        state, resp = extend_code(code, dt)
        Clock.schedule_once(partial(self.on_extend_response, state, resp))

    @mainthread
    def on_extend_response(self, state, r, *args):
        self.screen_widget.loading_dlg.dismiss()
        if state:
            if r.status_code == 200:
                Snackbar(text='Successfully Extended').show()
            else:
                Snackbar(text=r.json().get('response', ''), background_color=(.8, 0, .3, .5)).show()
        else:
            Snackbar(text='Failed to extend - {}'.format(r), background_color=(.8, 0, .3, .5)).show()

    def update_time(self):
        cur_dt = datetime.datetime.now()
        cur_dt = cur_dt + datetime.timedelta(hours=1)
        self.time_picker.set_value(cur_dt.strftime("%H:%M"))
        self.date_picker.set_value(cur_dt.strftime("%m/%d/%Y"))
