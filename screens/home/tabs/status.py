# -*- coding: iso8859-15 -*-
import datetime
import os
import threading
import time
from kivy.clock import Clock, mainthread
from kivy.lang import Builder

from kivymd.list import TwoLineListItem
from screens.home.tabs.base import BaseTab
from settings import INTERVAL_STATUS
from utils.common import logger
from utils.web import get_tickets_of_today
from widgets.list import SPStatusItem

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'status.kv'))


class StatusTab(BaseTab):

    index = 0
    b_stop = threading.Event()

    def on_enter_tab(self, *args):
        super(StatusTab, self).on_enter_tab(*args)
        self.b_stop.clear()
        threading.Thread(target=self._check_tickets).start()

    def _check_tickets(self):
        while not self.b_stop.isSet():
            s_time = time.time()
            r, tickets = get_tickets_of_today()
            if r:
                Clock.schedule_once(lambda dt: self._update_ticket_widgets(tickets))
            logger.info('Today\'s Tickets: {}'.format(tickets))
            elapsed = time.time() - s_time
            time.sleep(max(0, INTERVAL_STATUS - elapsed))

    @mainthread
    def _update_ticket_widgets(self, tickets):
        printed_tickets = [t for t in tickets if t['type'] != 'permanent']
        cur_tickets = [t for t in tickets if t['type'] == 'temporary']
        permanent_tickets = [t for t in tickets if t['type'] == 'permanent']
        onsite_permanent_tickets = [t for t in permanent_tickets if t.get('src') == 'entry']
        self.ids.cur_tickets.text = '{} Current Tickets'.format(len(cur_tickets) if cur_tickets else 'No')
        self.ids.printed_tickets.text = "{} Tickets Printed Today".format(
            len(printed_tickets) if printed_tickets else 'No')
        self.ids.onsite_tickets.text = "{} Onsite".format(
            len(onsite_permanent_tickets) if onsite_permanent_tickets else 'No')
        self.ids.permanent_tickets.text = '{} Permanent Tickets Used'.format(
            len(permanent_tickets) if permanent_tickets else 'No')

        self.ids.left_list.clear_widgets()
        self.ids.right_list.clear_widgets()

        for t in cur_tickets:
            reg_date = datetime.datetime.strptime(t['reg_date'], "%Y-%m-%dT%H:%M:%S.%f")
            exp_date = datetime.datetime.strptime(t['exp_date'], "%Y-%m-%dT%H:%M:%S.%f")
            txt = "%.4d %s" % (t['code'], '' if datetime.datetime.now() < exp_date else '(Expired)')
            self.ids.left_list.add_widget(
                SPStatusItem(
                    text=txt, text_right="Entry: {}".format(reg_date.strftime("%I:%M %P - %d/%m/%Y")),
                    _txt_left_pad=25, font_style="Subhead",
                ))

        for i, t in enumerate(onsite_permanent_tickets):
            last_date = datetime.datetime.strptime(t['last_date'], "%Y-%m-%dT%H:%M:%S.%f")
            self.ids.right_list.add_widget(
                TwoLineListItem(
                    text="{}  -  {:15}".format("%.4d" % t['code'], t['name']),
                    secondary_text='Entry: {}'.format(last_date.strftime("%I:%M %P - %d/%m/%Y")),
                    font_style="Subhead", _txt_right_pad=5, _txt_left_pad=10,
                ))

    def on_pre_leave(self, *args):
        self.b_stop.set()
        super(StatusTab, self).on_pre_leave(*args)
