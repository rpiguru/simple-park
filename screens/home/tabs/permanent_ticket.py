# -*- coding: iso8859-15 -*-

import os
from functools import partial

from kivy.clock import Clock
from kivy.lang import Builder

from kivymd.snackbar import Snackbar
from screens.home.tabs.base import BaseTab
from utils.db import get_all_permanent_tickets, add_permanent_ticket, remove_permanent_ticket
from widgets.dialog import YesNoDialog
from widgets.permanent_ticket import PermanentTicketItem, PermanentTicketDlg

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'permanent_ticket.kv'))


class PermanentTicketTab(BaseTab):

    index = 3

    def on_enter_tab(self, *args):
        Clock.schedule_once(lambda dt: self._update_screen())

    def _update_screen(self):
        tickets = get_all_permanent_tickets()
        container = self.ids.container
        container.clear_widgets()
        container.height = 10
        for t in tickets:
            wid = PermanentTicketItem(code=t['code'], name=t['name'])
            wid.bind(on_remove=self.on_remove_ticket)
            container.add_widget(wid)
            container.height += (wid.height + container.spacing)

    def on_btn_add(self):
        dlg = PermanentTicketDlg()
        dlg.bind(on_confirm=self.add_new_ticket)
        dlg.open()

    def add_new_ticket(self, *args):
        data = args[0].get_value()
        r, msg = add_permanent_ticket(code=data['code'], name=data['name'])
        if r:
            self._update_screen()
            Snackbar(text='Created a new permanent code, name: {}, code: {}'.format(data['name'], data['code'])).show()
            args[0].dismiss()
        else:
            Snackbar(text=msg, background_color=(.8, 0, .3, .5)).show()

    def on_remove_ticket(self, *args):
        dlg = YesNoDialog(message='Are you sure?')
        dlg.bind(on_confirm=partial(self.on_confirm_remove, args[0].code))
        dlg.open()

    def on_confirm_remove(self, code, *args):
        args[0].dismiss()
        remove_permanent_ticket(code)
        self._update_screen()
        Snackbar(text='Removed a permanent code: {}'.format(code)).show()
