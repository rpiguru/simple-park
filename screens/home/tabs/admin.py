# -*- coding: iso8859-15 -*-
import datetime
import os
from kivy.lang import Builder

from kivymd.snackbar import Snackbar
from screens.home.tabs.base import BaseTab, DatetimeMixin
from utils.common import get_config, update_config_file, set_backlight
from utils.time_util import update_system_datetime

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'admin.kv'))


class AdminTab(DatetimeMixin, BaseTab):

    index = 4

    def on_enter_tab(self, *args):
        super(AdminTab, self).on_enter_tab(*args)
        self.ids.time_limit.text = str(get_config()['time_limit'])
        self.ids.back_light.text = str(get_config()['back_light'])
        self.ids.ticket_word.text = get_config()['ticket_word']
        self.ids.brightness.value = get_config()['brightness']

    def on_btn_update_time_limit(self):
        conf = get_config()
        conf['time_limit'] = int(self.ids.time_limit.text)
        update_config_file(conf)
        Snackbar(text='Updated the time limit').show()

    def on_btn_update_ticket_word(self):
        conf = get_config()
        conf['ticket_word'] = self.ids.ticket_word.text
        update_config_file(conf)
        Snackbar(text='Updated the ticket word').show()

    def on_btn_update_datetime(self):
        dt_str = self.date_picker.text + ' ' + self.time_picker.text
        dt = datetime.datetime.strptime(dt_str, "%m/%d/%Y %H:%M")
        update_system_datetime(dt)
        Snackbar(text='Updated System DateTime - {}'.format(dt_str)).show()

    def on_btn_update_back_light(self):
        if len(self.ids.back_light.text) == 0:
            Snackbar(text='Invalid Back Light Value!', background_color=(.8, 0, .3, .5)).show()
            return
        conf = get_config()
        conf['back_light'] = int(self.ids.back_light.text)
        update_config_file(conf)
        Snackbar(text='Updated the Back Light').show()

    def on_brightness_changed(self):
        n = int(self.ids.brightness.value)
        set_backlight(n)
        c = get_config()
        c['brightness'] = n
        update_config_file(c)
