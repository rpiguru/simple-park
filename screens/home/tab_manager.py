# -*- coding: iso8859-15 -*-

from kivy.uix.screenmanager import ScreenManager, NoTransition

from screens.home.tabs.admin import AdminTab
from screens.home.tabs.controls import ControlsTab
from screens.home.tabs.extend_ticket import ExtendTicketTab
from screens.home.tabs.permanent_ticket import PermanentTicketTab
from screens.home.tabs.status import StatusTab

sm_settings = ScreenManager(transition=NoTransition())


tabs = {
    'status': StatusTab,
    'extend_ticket': ExtendTicketTab,
    'controls': ControlsTab,
    'permanent_ticket': PermanentTicketTab,
    'admin': AdminTab,
}
