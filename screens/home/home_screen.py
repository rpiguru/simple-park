import os
import time

from kivy.clock import Clock
from kivy.lang import Builder
from kivy.uix.screenmanager import NoTransition, SlideTransition
from screens.base import BaseScreen
from screens.home.tab_manager import tabs
from utils.common import logger

Builder.load_file(os.path.join(os.path.dirname(__file__), 'home_screen.kv'))


class HomeScreen(BaseScreen):

    show_back_button = False
    cur_tab = None
    start_tab = 'status'
    last_switch_time = 0

    def __init__(self, **kwargs):
        super(HomeScreen, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self.initialize())

    def on_enter(self, *args):
        if self.cur_tab is not None:
            self.cur_tab.on_enter()
        super(HomeScreen, self).on_enter(*args)

    def initialize(self):
        self.switch_tab(self.start_tab)

    def switch_tab(self, tab_name, tab_btn=None):
        """
        Go to given screen
        :param tab_btn: Tab button widget
        :param tab_name:     destination tab name
        :return:
        """
        try:
            if tab_name in tabs.keys():
                if time.time() - self.last_switch_time < .2:
                    logger.error('SP: Too early to switch tab')
                    return
                self.last_switch_time = time.time()
                sm = self.ids.sm
                tab = tabs[tab_name](screen_widget=self, name=tab_name)
                if tab_btn is None:
                    tab_btn = self.ids['btn_{}'.format(tab_name)]

                if self.cur_tab is None or tab_btn is None:
                    sm.transition = NoTransition()
                else:
                    sm.transition = SlideTransition()

                if self.cur_tab is None:
                    sm.switch_to(tab)
                    self.cur_tab = tab
                    tab_btn.on_tabbed(True)
                elif self.cur_tab.name != tab.name:
                    direction = 'left' if self.cur_tab.index < tab.index else 'right'
                    sm.switch_to(tab, direction=direction)
                    self.ids['btn_{}'.format(self.cur_tab.name)].on_tabbed(False)
                    self.cur_tab = tab
                    tab_btn.on_tabbed(True)
        except Exception as e:
            logger.exception('Failed to switch tab: {}'.format(e))

    def on_pre_leave(self, *args):
        self.cur_tab.on_pre_leave()
        super(HomeScreen, self).on_pre_leave(*args)
