#!/usr/bin/env bash

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

sudo apt-get install -y python-dev python-pip
sudo pip install -U pip

sudo pip install -U RPi.GPIO requests paho-mqtt keyboard

sudo apt-get install -y cups libcups2-dev libcupsimage2-dev g++ cups-client

sudo pip install pycups reportlab pypdf2 pyBarcode


cd ${cur_dir}/printer_driver
sudo bash TG2460H_CUPSDrv-201.sh
sudo bash VKP80III_CUPSDrv-202.sh

# Enable auto start
sudo apt-get install -y screen
sudo sed -i -- "s/^exit 0/screen -mS sp -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp -X stuff \"cd \/home\/pi\/simple-park\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp -X stuff \"python main_entry.py\\\\r\"\\nexit 0/g" /etc/rc.local

echo "========== Finished Installation, now rebooting... =========="
sudo reboot
