#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  echo "Must be root"
  exit
fi

# ========================== WiFi AP setup scripts ==========================
apt-get update

apt-get install -y dnsmasq hostapd

echo "denyinterfaces wlan0" >> /etc/dhcpcd.conf

# remove origin wlan0 setting scripts (remove from /etc/network/interfaces)
# sed -i -- 's/iface eth0 inet manual//g' /etc/network/interfaces
sed -i -- 's/allow-hotplug wlan0//g' /etc/network/interfaces
sed -i -- 's/iface wlan0 inet manual//g' /etc/network/interfaces
sed -i -- 's/allow-hotplug wlan1//g' /etc/network/interfaces
sed -i -- 's/iface wlan1 inet manual//g' /etc/network/interfaces
sed -i -- 's/    wpa-conf \/etc\/wpa_supplicant\/wpa_supplicant.conf//g' /etc/network/interfaces

cat >> /etc/network/interfaces <<EOF
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet static
	address 172.24.1.1
	netmask 255.255.255.0
	network 172.24.1.0
	broadcast 172.24.1.255
EOF

service dhcpcd restart
ifdown wlan0
ifup wlan0

sn="$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)"
sn=${sn:8:8}
echo ${sn}

cat > /etc/hostapd/hostapd.conf <<EOF
# This is the name of the WiFi interface we configured above
interface=wlan0

# Use the nl80211 driver with the brcmfmac driver
driver=nl80211

# This is the name of the network
ssid=SimplePark-${sn}

# Use the 2.4GHz band
hw_mode=g

# Use channel 1
channel=1

# Enable 802.11n
ieee80211n=1

# Enable WMM
wmm_enabled=1

# Enable 40MHz channels with 20ns guard interval
# ht_capab=[HT40][SHORT-GI-20][DSSS_CCK-40]

# Accept all MAC addresses
macaddr_acl=0

# Use WPA authentication
auth_algs=1

# Require clients to know the network name
ignore_broadcast_ssid=0

# Use WPA2
wpa=2

# Use a pre-shared key
wpa_key_mgmt=WPA-PSK

# The network passphrase
wpa_passphrase=simple_park

# Use AES, instead of TKIP
rsn_pairwise=CCMP
EOF

sed -i -- '/#DAEMON_CONF=""/c\DAEMON_CONF="/etc/hostapd/hostapd.conf"' /etc/default/hostapd

cat > /etc/dnsmasq.conf <<EOF
interface=wlan0                 # Use interface wlan0
listen-address=172.24.1.1       # Explicitly specify the address to listen on
bind-interfaces                 # Bind to the interface to make sure we aren't sending things elsewhere
server=8.8.8.8                  # Forward DNS requests to Google DNS
domain-needed                   # Don't forward short names
bogus-priv                      # Never forward addresses in the non-routed address spaces.
dhcp-range=172.24.1.50,172.24.1.150,12h # Assign IP addresses between 172.24.1.50 and 172.24.1.150 with a 12 hour lease time
EOF

sudo service hostapd start

echo "Successfully installed WiFi AP, SSID is SimplePark-${sn}"
