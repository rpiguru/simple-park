#!/usr/bin/env bash

sudo apt-get install -y python-dev python-pip
sudo pip install -U pip

sudo pip install -U RPi.GPIO requests paho-mqtt keyboard

# Enable auto start
sudo apt-get install -y screen
sudo sed -i -- "s/^exit 0/screen -mS sp -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp -X stuff \"cd \/home\/pi\/simple-park\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp -X stuff \"python main_exit.py\\\\r\"\\nexit 0/g" /etc/rc.local

echo "========== Finished Installation, now rebooting... =========="
sudo reboot
