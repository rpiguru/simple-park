#!/usr/bin/env bash

echo "========== Setting up Reception =========="
sudo apt-get update

# Setup WiFi AP
cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"

echo "Install Kivy now..."
sudo apt-get install -y python libpython-dev python-pip
sudo apt-get install -y libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev pkg-config libgl1-mesa-dev
sudo apt-get install -y libgles2-mesa-dev python-setuptools libgstreamer1.0-dev git-core gstreamer1.0-plugins-bad
sudo apt-get install -y gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-omx
sudo apt-get install -y gstreamer1.0-alsa python-dev libmtdev-dev xclip

sudo pip install -U pip
sudo pip install Cython==0.25.2

sudo pip install git+https://github.com/kivy/kivy.git@master

sudo apt-get install -y libjpeg-dev python-pil
sudo pip install -r ${cur_dir}/../requirements.txt

sudo apt-get install -y mosquitto mosquitto-clients

# Enable SSH
sudo touch /boot/ssh

# Increase GPU memory size
echo "gpu_mem=512" | sudo tee -a /boot/config.txt

echo "Disabling the booting logo..."
echo "disable_splash=1" | sudo tee -a /boot/config.txt
sudo sed -i -- "s/$/ logo.nologo quiet loglevel=3 vt.global_cursor_default=0 systemd.show_status=0/" /boot/cmdline.txt
sudo sed -i -- "s/console=tty1/console=tty3/" /boot/cmdline.txt

sudo bash ${cur_dir}/install_mongodb.sh

# Install SSL library
sudo apt-get install -y libssl-dev
cd /tmp
wget http://ftp.nl.debian.org/debian/pool/main/o/openssl/libssl1.0.0_1.0.1t-1%2Bdeb8u7_armhf.deb
sudo spkg -i libssl1.0.0_1.0.1t-1%2Bdeb8u7_armhf.deb

# Disable the blinking cursor
sudo sed -i -- "s/^exit 0/TERM=linux setterm -foreground black >\/dev\/tty0\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/TERM=linux setterm -clear all >\/dev\/tty0\\nexit 0/g" /etc/rc.local

# Enable auto start
sudo apt-get install screen
sudo sed -i -- "s/^exit 0/screen -mS sp_kivy -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp_kivy -X stuff \"cd \/home\/pi\/simple-park\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp_kivy -X stuff \"python main_reception.py\\\\r\"\\nexit 0/g" /etc/rc.local

sudo sed -i -- "s/^exit 0/screen -mS sp_web -d\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp_web -X stuff \"cd \/home\/pi\/simple-park\\\\r\"\\nexit 0/g" /etc/rc.local
sudo sed -i -- "s/^exit 0/screen -S sp_web -X stuff \"python simple_park_server.py\\\\r\"\\nexit 0/g" /etc/rc.local

echo "========== Finished Installation, now rebooting... =========="
sudo reboot
