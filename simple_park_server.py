import datetime
import logging.config
import random
from flask import Flask
from flask import jsonify
from flask import request
from pymongo import MongoClient

from settings import MONGO_DB, MONGO_COLLECTION, WEB_PORT, MONGO_HOST
from utils.common import get_config, logger

app = Flask(__name__)

mongo_client = MongoClient(MONGO_HOST, 27017)
mongo_db = mongo_client[MONGO_DB]
col = mongo_db[MONGO_COLLECTION]


def handle_response(body, status_code):
    resp = jsonify(body)
    resp.status_code = status_code
    return resp


@app.route('/create', methods=['POST'])
def create_code():
    existing_codes = []
    for s in col.find():
        existing_codes.append(s.get('code'))
    while True:
        code = random.randint(1, 9999)
        if code not in existing_codes:
            break

    code_type = request.json.get('type', 'temporary')
    time_limit = get_config().get('time_limit', 60)
    data = dict(code=code,
                reg_date=datetime.datetime.now(),
                time_limit=time_limit,
                exp_date=datetime.datetime.now() + datetime.timedelta(minutes=time_limit),
                ticket_word=get_config().get('ticket_word', 'Toowoomba Medical Centre'),
                type=code_type
                )
    col.insert(data)

    data.pop('_id')
    data['reg_date'] = data['reg_date'].isoformat()
    data['exp_date'] = data['exp_date'].isoformat()

    logger.info('Created a ticket - {}'.format(data))
    return handle_response(data, 200)


@app.route('/mark', methods=['POST'])
def mark_code():
    code = request.json['code']
    doc = col.find_one({"code": code})
    if doc:
        if doc['type'] == 'temporary':
            doc['type'] = 'removed'
            doc['del_date'] = datetime.datetime.now()
            col.update({'_id': doc['_id']}, {"$set": doc}, upsert=False)
            logger.info('Marked `removed` a temporary ticket - {}'.format(doc))
        elif doc['type'] == 'permanent':
            logger.info('A permanent code is used - {}'.format(doc['code']))
        else:
            return handle_response({'response': 'Already marked code'}, 400)
        return handle_response({'response': 'OK'}, 200)
    else:
        return handle_response({'response': 'Not Found Code'}, 404)


@app.route('/remove', methods=['POST'])
def remove_code():
    code = request.json['code']
    doc = col.find_one({"code": code})
    if doc:
        if doc['type'] == 'permanent':
            logger.info('A permanent code cannot be removed from Entry/Exit - {}'.format(doc['code']))
        else:
            col.delete_one({'_id': doc['_id']})
            logger.info('Removed a temporary ticket - {}'.format(doc))
        return handle_response({'response': 'OK'}, 200)
    else:
        return handle_response({'response': 'Not Found Code'}, 404)


@app.route('/extend', methods=['POST'])
def extend_code():
    code = request.json['code']
    new_dt = request.json['exp_date']

    doc = col.find_one({"code": code})
    if doc:
        if doc['type'] != 'permanent':
            doc['exp_date'] = datetime.datetime.strptime(new_dt, "%Y-%m-%dT%H:%M:%S")
            col.update({'_id': doc['_id']}, {"$set": doc}, upsert=False)
            logger.info('Extended a ticket({}) - {}'.format(code, new_dt))
            return handle_response({'response': 'OK'}, 200)
        else:
            return handle_response({'response': 'Permanent Code Cannot be extended'}, 400)
    else:
        return handle_response({'response': 'Not Found Code'}, 404)


@app.route('/info', methods=['POST'])
def get_code_info():
    code = request.json['code']
    doc = col.find_one({"code": code})
    if doc:
        doc.pop('_id')
        doc['reg_date'] = doc['reg_date'].isoformat()
        if doc.get('exp_date'):
            doc['exp_date'] = doc['exp_date'].isoformat()
        return handle_response({'response': doc}, 200)
    else:
        return handle_response({'response': 'Not Found Code'}, 404)


@app.route('/locked', methods=['POST'])
def get_locked_state():
    position = request.json['position']
    doc = col.find_one({"code": 'lock_{}'.format(position)})
    return handle_response({'response': doc.get('state', False) if doc else False}, 200)


@app.route('/permanent', methods=['POST'])
def switch_permanent_source():
    code = request.json['code']
    src = request.json['source']
    doc = col.find_one({"code": code})
    if doc:
        if doc['type'] == 'permanent':
            doc['src'] = src
            doc['last_date'] = datetime.datetime.now()
            col.update({'_id': doc['_id']}, {"$set": doc}, upsert=False)
            logger.info('Switched a permanent ticket - {}'.format(doc))
            return handle_response({'response': 'OK'}, 200)
        else:
            return handle_response({'response': 'Invalid code, should be permanent one.'}, 400)
    else:
        return handle_response({'response': 'Not Found Code'}, 404)


@app.route('/get_tickets_of_today', methods=['GET'])
def get_tickets_of_today():
    tickets = []
    now = datetime.date.today()
    for t in list(col.find()):
        if t.get('type'):
            if t['type'] == 'permanent':
                d = t.get('last_date')  # Use `last_date` to compare
            else:
                d = t.get('reg_date')   # Use `reg_date` to compare

            if d and now.year == d.year and now.month == d.month and now.day == d.day:
                t.pop('_id')
                for _k in t.keys():
                    if _k.endswith('_date'):
                        t[_k] = t[_k].isoformat()
                tickets.append(t)
    return handle_response({'response': tickets}, 200)


if __name__ == "__main__":
    logger.info('========== Starting SimplePark Server ==========')

    log = logging.getLogger('werkzeug')
    log.setLevel(logging.DEBUG)

    app.run(host='0.0.0.0', port=WEB_PORT)
