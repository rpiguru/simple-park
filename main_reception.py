"""
Python script to manage RPi at Reception
"""

import gc
import os
import subprocess
import traceback

# Import this before Kivy
from kivy.properties import DictProperty
from kivy.uix.modalview import ModalView

import conf.config_before
from kivy.app import App
# Import after Kivy
import conf.config_kivy
import widgets.factory_reg

from kivy.clock import Clock
from paho.mqtt.client import Client
from kivy.uix.screenmanager import SlideTransition, NoTransition
from kivy.base import ExceptionHandler, ExceptionManager
from kivymd.theming import ThemeManager
from screens.screen_manager import screens, sm
from utils.common import is_rpi, get_free_gpu_size, disable_screen_saver, check_running_proc, logger, set_backlight, \
    get_config
from settings import *
from utils.db import delete_expired_tickets


_cur_dir = os.path.dirname(os.path.realpath(__file__))

mqtt_client = Client()


class SPExceptionHandler(ExceptionHandler):

    def handle_exception(self, exception):
        logger.exception(exception)
        _app = App.get_running_app()
        _app.save_exception(traceback.format_exc(limit=30))
        _app.switch_screen('error_screen')
        return ExceptionManager.PASS


ExceptionManager.add_handler(SPExceptionHandler())


class SimpleParkReceptionApp(App):

    current_screen = None
    exception = None
    theme_cls = ThemeManager()
    clk_back_light = None
    locked_up_state = DictProperty()

    def build(self):
        mqtt_client.on_connect = on_mqtt_connected
        mqtt_client.on_message = on_mqtt_messaged
        mqtt_client.connect(MQTT_HOST, 1883)
        mqtt_client.loop_start()

        Clock.schedule_interval(lambda dt: delete_expired_tickets(), 60)

        self.switch_screen('home_screen')
        self._start_timer()
        return sm

    def switch_screen(self, screen_name, direction=None, duration=.3):
        if sm.has_screen(screen_name):
            sm.current = screen_name
        else:
            screen = screens[screen_name](name=screen_name)
            if direction:
                sm.transition = SlideTransition(direction=direction, duration=duration)
            else:
                sm.transition = NoTransition()

            sm.switch_to(screen)
            msg = ' :: GPU - {}'.format(get_free_gpu_size()) if is_rpi() else ''
            logger.info('SimplePark: ===== Switched to {} screen {} ====='.format(screen_name, msg))
            if self.current_screen:
                sm.remove_widget(self.current_screen)
                del self.current_screen
                gc.collect()
            self.current_screen = screen

    def save_exception(self, ex):
        self.exception = ex

    def get_exception(self):
        return self.exception

    def on_touched(self):
        if self.clk_back_light is None:
            set_backlight(get_config()['brightness'])
        else:
            self.clk_back_light.cancel()
        self._start_timer()

    def _on_timeout(self):
        set_backlight(0)
        self.clk_back_light = None
        # Close any opened dialog
        for wid in self.root_window.children:
            if isinstance(wid, ModalView):
                wid.dismiss()

    def on_modal_touched(self, *args):
        self.on_touched()

    def _start_timer(self):
        self.clk_back_light = Clock.schedule_once(lambda dt: self._on_timeout(), get_config()['back_light'])

    @staticmethod
    def publish_mqtt_message(topic, payload):
        logger.info('MQTT: Publishing a message, topic: `{}`, payload: `{}`'.format(topic, payload))
        mqtt_client.publish(topic=topic, payload=payload)


def on_mqtt_messaged(*args):
    topic = args[2].topic
    msg = args[2].payload.decode('utf-8')
    logger.info('Received MQTT message, topic: {}, payload: {}'.format(topic, msg))


def on_mqtt_connected(client, userdata, flags, rc):
    logger.info('MQTT: Connected to the Broker, Host: {}, Port: 1883, UserData: {}, Flags: {}, Result code: {}'.format(
        MQTT_HOST, userdata, flags, rc))


if __name__ == '__main__':

    if not check_running_proc('mongo') and is_rpi():
        logger.warning('MongoDB service is not running... repairing...')
        os.system('sudo service mongodb stop')
        os.system('sudo rm /tmp/mongodb-27017.sock')
        _p = subprocess.Popen('sudo -u mongodb mongod --repair --dbpath /var/lib/mongodb/', shell=True,
                              stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        _p.communicate()
        _p.wait()
        subprocess.Popen('service mongodb start', shell=True)

    set_backlight(get_config()['brightness'])
    disable_screen_saver()

    app = SimpleParkReceptionApp()
    app.run()
