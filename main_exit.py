"""
Python script to manage RPi at exit column
"""
import datetime
import threading
import time
import keyboard
from paho.mqtt.client import Client
from settings import PIN_LOOP, MQTT_HOST, MQTT_TOPIC_EXIT_RAISE, MQTT_TOPIC_EXIT_LOCK_UP, \
    MQTT_TOPIC_EXIT_UNLOCK_UP, MASTER_CODE
from utils.arm import raise_arm, move_arm
from utils.barcodes import LOWER_CASE, UPPER_CASE
from utils.common import logger
from utils.web import get_code_info, mark_code, get_locked_state, switch_permanent_code
import RPi.GPIO as GPIO


DEBUG = True

mqtt_client = Client()


class SimpleParkExit(threading.Thread):

    armed = False
    is_ticket_scanned = False
    is_busy = threading.Event()
    lock = threading.RLock()

    buf = ""

    def __init__(self):
        super(SimpleParkExit, self).__init__()

    def _check_loop_pin(self):
        while True:
            pin_state = GPIO.input(PIN_LOOP)
            with self.lock:
                if self.armed != pin_state:
                    if pin_state:
                        logger.info('=== Vehicle is detected!')
                    else:
                        logger.info('=== Vehicle is disappeared!')
                        self.is_ticket_scanned = False
                    self.armed = pin_state
            time.sleep(.5)

    def on_pressed(self, event):
        """
        Callback when a key is pressed
        :param event: keyboard.KeyboardEvent instance
        :return:
        """
        code = event.name

        if DEBUG:
            print('Code : `{}`'.format(code))

        if code == 'enter':
            print('==== Barcode is scanned! - {}'.format(self.buf))
            self.parse_code(self.buf)
            self.buf = ""
        elif code in LOWER_CASE or code in UPPER_CASE:
            self.buf = self.buf + code
        else:
            print('Unknown code - {}'.format(code))

    def parse_code(self, code):
        logger.info('Barcode is scanned - {}'.format(code))
        if code == MASTER_CODE:
            raise_arm('exit')
            return

        try:
            code = int(code)
        except ValueError:
            logger.error('Invalid code - {}, type: {}'.format(code, type(code)))
            return

        with self.lock:
            if not self.armed:
                logger.error('Barcode is scanned({}), but the LOOP is not detected!'.format(code))
                return

        with self.lock:
            if self.is_ticket_scanned:
                logger.error('Ticket can be scanned one time only.')
                return

        if get_locked_state('exit'):
            logger.warning('The gate is locked!')
        else:
            r = get_code_info(code)
            if r.status_code == 200:
                info = r.json()['response']
                logger.info('Code info: {}'.format(info))
                if info['type'] == 'removed':
                    logger.error('Already deleted!')
                elif info['type'] == 'permanent':
                    if info.get('src') != 'exit':
                        with self.lock:
                            self.is_ticket_scanned = True
                        state, resp = switch_permanent_code(code=code, src='exit')
                        if state and resp.get('response') == 'OK':
                            raise_arm('exit')
                        else:
                            logger.error('Failed to change the status of the permanent code - {}'.format(resp))
                    else:
                        logger.error('Cannot raise arm. You are scanning a permanent code twice!')
                else:
                    try:
                        exp_date = datetime.datetime.strptime(info['exp_date'], "%Y-%m-%dT%H:%M:%S.%f")
                    except ValueError:
                        exp_date = datetime.datetime.strptime(info['exp_date'], "%Y-%m-%dT%H:%M:%S")
                    if datetime.datetime.now() < exp_date:
                        with self.lock:
                            self.is_ticket_scanned = True
                        raise_arm('exit')
                        state, r = mark_code(code)
                        if state:
                            logger.info('Successfully removed the code.')
                    else:
                        logger.error('Expired Ticket!!!')
            else:
                logger.error('Cannot find details of the code!')

    def run(self):
        keyboard.on_press(self.on_pressed)
        threading.Thread(target=self._check_loop_pin).start()
        keyboard.wait()


def on_mqtt_messaged(*args):
    topic = args[2].topic
    msg = args[2].payload.decode('utf-8')
    logger.info('Received MQTT message, topic: {}, payload: {}'.format(topic, msg))
    if topic == MQTT_TOPIC_EXIT_RAISE:
        raise_arm(gate='exit')
    elif topic == MQTT_TOPIC_EXIT_LOCK_UP:
        move_arm(gate='exit', value=True)
    elif topic == MQTT_TOPIC_EXIT_UNLOCK_UP:
        move_arm(gate='exit', value=False)


def on_mqtt_connected(client, userdata, flags, rc):
    logger.info('MQTT: Connected to the Broker, Host: {}, Port: 1883, UserData: {}, Flags: {}, Result code: {}'.format(
        MQTT_HOST, userdata, flags, rc))
    mqtt_client.subscribe(MQTT_TOPIC_EXIT_RAISE)
    mqtt_client.subscribe(MQTT_TOPIC_EXIT_LOCK_UP)
    mqtt_client.subscribe(MQTT_TOPIC_EXIT_UNLOCK_UP)


if __name__ == '__main__':

    time.sleep(10)

    logger.info('========== Starting SimplePark Exit Program ==========')

    mqtt_client.on_connect = on_mqtt_connected
    mqtt_client.on_message = on_mqtt_messaged
    mqtt_client.connect(MQTT_HOST, 1883)
    mqtt_client.loop_start()

    spe = SimpleParkExit()
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_LOOP, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    spe.start()
