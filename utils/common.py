# -*- coding: iso8859-15 -*-
import json
import os
import platform
import shutil
import subprocess
import signal
import gc
import logging.config


def is_rpi():
    if platform.system() == 'Linux':
        return 'arm' in os.uname()[4]
    else:
        return False


def get_serial():
    """
    Get serial number of the device
    :return:
    """
    if is_rpi():
        cpuserial = "0000000000000000"
        f = open('/proc/cpuinfo', 'r')
        for line in f:
            if line[0:6] == 'Serial':
                cpuserial = line[10:26].lstrip('0')
        f.close()
        return cpuserial
    else:
        return '12345678'


_cur_dir = os.path.dirname(os.path.realpath(__file__))
_json_file = os.path.join(_cur_dir, os.pardir, 'config_{}.json'.format(get_serial()))
if not os.path.exists(_json_file):
    print('No JSON Config File Found! Recovering the default one...')
    shutil.copy(os.path.join(_cur_dir, os.pardir, 'config.json'), _json_file)


logging.config.fileConfig(os.path.join(_cur_dir, os.pardir, 'conf', 'logging.ini'))
logger = logging.getLogger('SimplePark')


def update_config_file(data):
    json_data = json.loads(open(_json_file).read())
    json_data.update(data)
    with open(_json_file, 'w') as outfile:
        json.dump(json_data, outfile, indent=2)


def get_config():
    return json.loads(open(_json_file).read())


def kill_process_by_name(proc_name):
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.decode().splitlines():
        if proc_name in line:
            pid = int(line.split(None, 1)[0])
            print('Found PID({}) of `{}`, killing...'.format(pid, proc_name))
            os.kill(pid, signal.SIGKILL)


def number_to_ordinal(n):
    """
    Convert number to ordinal number string
    """
    return "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])


def get_free_gpu_size():
    gc.collect()
    if is_rpi():
        pipe = os.popen('sudo vcdbg reloc stats | grep "free memory"')
        data = pipe.read().strip()
        pipe.close()
        return data
    else:
        return 0


def disable_screen_saver():
    if is_rpi():
        os.system('sudo sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')


def get_screen_resolution():
    """
    Get resolution of the screen
    :return:
    """
    if is_rpi():
        try:
            pipe = os.popen('fbset -s')
            data = pipe.read().strip()
            pipe.close()
            for line in data.splitlines():
                if line.startswith('mode'):
                    w, h = [int(p) for p in line.split('"')[1].split('x')]
                    return w, h
        except OSError:
            return 800, 480
    else:
        return 800, 480


def check_running_proc(proc_name):
    """
    Check if a process is running or not
    :param proc_name:
    :return:
    """
    is_running = False
    try:
        if len(os.popen("ps -aef | grep -i '%s' "
                        "| grep -v 'grep' | awk '{ print $3 }'" % proc_name).read().strip().splitlines()) > 0:
            is_running = True
    except Exception as e:
        print('Failed to get status of the process({}) - {}'.format(proc_name, e))
    return is_running


def set_backlight(n):
    logger.info('Setting backlight - {}'.format(n))
    if is_rpi():
        os.system('sudo sh -c "echo {} > /sys/class/backlight/rpi_backlight/brightness"'.format(n))
