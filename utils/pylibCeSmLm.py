"""
    Get FULL Status of the printer.

    See page 14 of the manual(`VKP80III Commands.pdf`) for more details.

"""

import os
import subprocess

_cur_dir = os.path.dirname(os.path.realpath(__file__))


def _get_status():
    p = subprocess.Popen('cd {}; ./ReadStatusSimple'.format(os.path.join(_cur_dir, 'CeSmLm_armv7-105', 'bin')),
                         shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    print('Getting printer status: {}'.format(out))
    if err:
        return False, err
    else:
        if 'Error' in out:
            return False, str(out).split('Error:')[1]
        result = str(out).split('Printer Status=')[1].split('|')
        return True, result


def ticket_exist():
    """
    Check if a ticket was withdrawn or not.
    :return:
    """
    r, status = _get_status()
    if r:
        # The 3rd byte is the `paper status` byte.
        bits = '{0:08b}'.format(int(status[2], 16))
        # The 3rd bit is the ticket presence bit.
        if bits[2] == '1':
            return True
    else:
        print(status)
        return False


if __name__ == '__main__':
    print("Status: ", _get_status())
    print("Ticket exist? ", ticket_exist())
