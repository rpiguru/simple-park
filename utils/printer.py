import datetime
import platform
import cups
from reportlab.pdfgen import canvas
import barcode
from barcode.writer import *
from reportlab.lib.units import mm
import PyPDF2


PRINTER_NAME = "VKP80III"
_cur_dir = os.path.dirname(os.path.realpath(__file__))


def is_rpi():
    if platform.system() == 'Linux':
        return 'arm' in os.uname()[4]
    else:
        return False


class MyBarcodeWriter(ImageWriter):

    def __init__(self):
        super(MyBarcodeWriter, self).__init__()
        self.dpi = 300

    def _paint_text(self, xpos, ypos):
        """
        Re-define this function to avoid printing barcode value below barcode image.
        :param xpos:
        :param ypos:
        :return:
        """
        pass


def print_file(file_name, copies=1):
    if is_rpi():
        try:
            conn = cups.Connection()
            printers = conn.getPrinters()
            printer_name = get_current_printer_name(printers)
            result = conn.printFile(printer_name, file_name, 'Print label', {'copies': str(copies), 'collate': 'true'})
            print 'Printing `{}` by {}: {} '.format(file_name, printer_name, result)
            return result
        except Exception as e:
            print 'Failed to print: {}'.format(e)
    else:
        print ''


def get_current_printer_name(printers):
    """
    Return currently connected printer name
    :return:
    """
    for _key, _item in printers.items():
        if str(_item['device-uri']).startswith('usb://') and PRINTER_NAME in _key:
            return _key


def create_barcode_png(code='1234'):
    CODE39 = barcode.get_barcode_class('code39')
    ean = CODE39(code, writer=MyBarcodeWriter(), add_checksum=False)
    fullname = ean.save(code)
    return fullname


def print_ticket(data):
    """
    Compose the Barcode image and print it with thermal printer.
    :param data:
            {
                'code': 1234,
                'reg_date': "2018-06-15T17:25:42.676314",
                'exp_date': "2018-06-15T17:35:42.676314",
                'time_limit': 60,
                'ticket_word': 'Your Car Park',
                'type': 'temporary'
            }
    :return:
    """
    # Create read_barcode image
    barcode_img = create_barcode_png("%.4d" % data['code'])

    width, height = 80 * mm, 100 * mm

    file_name = '/tmp/{}.pdf'.format(datetime.datetime.now().isoformat().replace(':', '-'))
    # file_name = 'test.pdf'

    result_name = file_name.replace('.pdf', '_rotated.pdf')

    c = canvas.Canvas(file_name, pagesize=(width, height))

    # c.setFillColorRGB(0, 0, 255)

    # Draw the logo image
    logo_img = os.path.join(_cur_dir, os.pardir, 'assets', 'images', 'logo-simplepark.jpg')
    c.drawImage(logo_img, 7, 175, width=200, height=50)

    # Draw the ticket word and `Ticket valid until:`
    c.setFontSize(12)
    c.drawString(67, 158, data['ticket_word'])
    c.drawString(61, 141, 'Ticket valid until:')

    # Draw the expiration text
    c.setFont('Helvetica-Bold', 14)
    exp_date = datetime.datetime.strptime(data['exp_date'], "%Y-%m-%dT%H:%M:%S.%f")
    c.drawString(36, 123, exp_date.strftime("%I:%M%P - %d/%m/%Y"))

    # Draw downward arrow
    down_img = os.path.join(_cur_dir, os.pardir, 'assets', 'images', 'Ic_arrow_drop_down_48px.jpg')
    c.drawImage(down_img, 60, 76, width=90, height=45)

    c.setFont('Helvetica', 11)
    c.drawString(53, 63, 'Pin Code:')

    c.setFont('Helvetica-Bold', 20)
    c.drawString(115, 61, str("%.4d" % data['code']))

    c.setFont('Helvetica', 10)
    exp_date = datetime.datetime.strptime(data['reg_date'], "%Y-%m-%dT%H:%M:%S.%f")
    c.drawString(25, 47, exp_date.strftime("Time of Entry: %I:%M%P - %d/%m/%Y"))

    # Draw the barcode image
    c.drawImage(barcode_img, 16, -37, width=180, height=80)

    c.save()
    os.remove(barcode_img)

    rotate_pdf(file_name, result_name)

    print_file(result_name)


def rotate_pdf(original, result):
    pdf_in = open(original, 'rb')
    pdf_reader = PyPDF2.PdfFileReader(pdf_in)
    pdf_writer = PyPDF2.PdfFileWriter()

    for i in range(pdf_reader.numPages):
        page = pdf_reader.getPage(i)
        page.rotateClockwise(180)
        pdf_writer.addPage(page)

    pdf_out = open(result, 'wb')
    pdf_writer.write(pdf_out)
    pdf_out.close()
    pdf_in.close()


if __name__ == '__main__':
    # create_barcode_png()
    print_ticket({
        'code': 1234,
        'reg_date': "2018-06-15T17:25:42.676314",
        'exp_date': "2018-06-15T17:35:42.676314",
        'time_limit': 60,
        'ticket_word': 'Your Car Park',
        'type': 'temporary'
    })
