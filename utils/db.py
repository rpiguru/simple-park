import datetime

from pymongo import MongoClient

from settings import MONGO_HOST, MONGO_DB, MONGO_COLLECTION
from utils.common import get_config, logger

mongo_client = MongoClient(MONGO_HOST, 27017)
mongo_db = mongo_client[MONGO_DB]
col = mongo_db[MONGO_COLLECTION]


def get_all_permanent_tickets():
    return list(col.find({'type': 'permanent'}))


def add_permanent_ticket(code, name):
    doc = col.find_one({'code': code})
    if doc:
        msg = 'Failed to add permanent ticket({}), already exists.'.format(code)
        logger.error(msg)
        return False, msg
    else:
        data = dict(code=code,
                    reg_date=datetime.datetime.now(),
                    ticket_word=get_config().get('ticket_word', 'Toowoomba Medical Centre'),
                    type='permanent',
                    name=name
                    )
        col.insert(data)
        return True, 'OK'


def remove_permanent_ticket(code):
    col.remove({'code': code})


def delete_expired_tickets():
    """
    Temporary pin codes must be deleted after 7 days.
    :return:
    """
    tickets = list(col.find({'type': 'removed'}))
    for t in tickets:
        if (datetime.datetime.now() - t['del_date']).days > 7:
            logger.info('Found an old ticket, removing - {}'.format(t))
            col.remove({'_id': t['_id']})


def lock_gate(pos, val):
    doc = col.find_one({'code': 'lock_{}'.format(pos)})
    if doc:
        doc['state'] = val
        col.update({'_id': doc['_id']}, {"$set": doc}, upsert=False)
    else:
        col.insert({'code': 'lock_{}'.format(pos), 'state': val})


if __name__ == '__main__':

    add_permanent_ticket(2345, 'Mattias')

    print(get_all_permanent_tickets())

    delete_expired_tickets()
