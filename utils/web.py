import json
import requests

from settings import WEB_HOST, WEB_PORT
from utils.common import logger


def get_locked_state(pos='entry'):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'position': pos}
        resp = requests.post('http://{}:{}/locked'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return resp.json().get('response')
    except Exception as e:
        logger.exception('Failed to get lock state of {} - {}'.format(pos, e))
        return True


def create_code(code_type='temporary'):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'type': code_type}
        resp = requests.post('http://{}:{}/create'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return resp.json()
    except Exception as e:
        logger.exception('Failed to create a new code - {}'.format(e))


def extend_code(code, dt):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'code': code, 'exp_date': dt.isoformat()}
        resp = requests.post('http://{}:{}/extend'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return True, resp
    except Exception as e:
        logger.exception('Failed to extend code - {}'.format(e))
        return False, e.__class__.__name__


def get_code_info(code):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'code': code}
        resp = requests.post('http://{}:{}/info'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return resp
    except Exception as e:
        logger.exception('Failed to get code info - {}'.format(e))


def mark_code(code):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'code': code}
        resp = requests.post('http://{}:{}/mark'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return True, resp.json()
    except Exception as e:
        logger.exception('Failed to mark a code - {}'.format(e))
        return False, e.__class__.__name__


def remove_code(code):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'code': code}
        resp = requests.post('http://{}:{}/remove'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return True, resp.json()
    except Exception as e:
        logger.exception('Failed to remove a code - {}'.format(e))
        return False, e.__class__.__name__


def switch_permanent_code(code=0, src='entry'):
    try:
        headers = {'Content-Type': 'application/json'}
        data = {'code': code, 'source': src}
        resp = requests.post('http://{}:{}/permanent'.format(WEB_HOST, WEB_PORT), headers=headers, data=json.dumps(data),
                             timeout=1)
        return True, resp.json()
    except Exception as e:
        logger.exception('Failed to remove a new code - {}'.format(e))
        return False, e.__class__.__name__


def get_tickets_of_today():
    try:
        headers = {'Content-Type': 'application/json'}
        resp = requests.get('http://{}:{}/get_tickets_of_today'.format(WEB_HOST, WEB_PORT), headers=headers, timeout=1)
        return True, resp.json().get('response', [])
    except Exception as e:
        logger.exception('Failed to get tickets of today - {}'.format(e))
        return False, e.__class__.__name__


if __name__ == '__main__':

    print(create_code())
