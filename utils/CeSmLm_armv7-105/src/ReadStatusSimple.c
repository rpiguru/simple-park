#include <stdio.h>
#include <assert.h>

// include libCeSmLm headers
#include "include/CeSmLm.h"
#include "include/ComErrors.h"

// define read buffer max size
#define READ_BUFFER_SIZE      2024 // 2k

int main()
{
  unsigned long dwRead = 0;
  unsigned long dwResult = 0;
  unsigned char buffer[READ_BUFFER_SIZE];
  char* str_ver;

  str_ver = GetLibVers();
//  printf("CeSmLm.so version is : %s\n", str_ver);

  // initialize th buffer
  bzero(buffer, READ_BUFFER_SIZE);

  // init and open the usb status device
  // printer :              vid      pid    serial number
  dwResult = openUSBDevice(0x0DD4, 0x0205, NULL);

  // check openUSBDevice result if is not ok exit and show error number see the file ComErrors.h
  if (dwResult != SUCCESS)
  {
    printf("Error: Failed to open the device %lu\n", dwResult);
    exit(-1);
  }

  dwResult = GetPrinterStatus(buffer, READ_BUFFER_SIZE, &dwRead);
  if (dwResult != SUCCESS)
  {
    printf("Error: Failed to get printer status %lu\n", dwResult);
    closeUSBDevice();
    exit(-1);
  }

  // show hex printer response
  printf("Printer Status=%x|%x|%x|%x|%x|%x", buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);

  closeUSBDevice();

  return 0;
}
