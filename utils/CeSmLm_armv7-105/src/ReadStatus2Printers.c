/***************************************************************************
                          ReadStatus.c  -  description
                             -------------------
    copyright            : (C) 2009 by Custom Engineering S.p.A
    email                : support @custom.it
    
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <time.h> 
#include <stdio.h>
#include <assert.h>
#include <sys/time.h>

// include libCeSmLm headers
#include "../include/CeSmLm.h"
#include "../include/ComErrors.h"

// define read buffer max size
#define READ_BUFFER_SIZE      2024 // 2k

int iPrinterID1 = 0;
char *strSerialNumberPrinterID1 = (char *)"VKP80III PRN Num.: 0";
int m_bOffLineStatusPrinterID1 = 1;


int iPrinterID2 = 0;
char *strSerialNumberPrinterID2 = (char *)"VKP80III PRN Num.: 1";
int m_bOffLineStatusPrinterID2 = 1;

long lPrinterVid = 0x0DD4;
long lPrinterPid = 0x0205;
/*

  checkStatus:  
  this function process the printer status buffer and show error or interestig status

*/

void checkStatus(unsigned char *StatusBuffer)
{
	// test if the printer response is ok
	if (!(StatusBuffer[0] & 0x10))   printf("			- error full status\n");
	if (!(StatusBuffer[1] & 0x0F))   printf("			- error full status\n");
	
	// paper status:
	if (StatusBuffer[2] & 0x01)   printf("				- No paper\n");
	if (StatusBuffer[2] & 0x04)   printf("				- Near paper end\n");
	if (StatusBuffer[2] & 0x20)   printf("				- Ticket Out\n");
	
	// user status:
	if (StatusBuffer[3] & 0x03)   printf("				- Cover opened\n");
	if (StatusBuffer[3] & 0x04)   printf("				- Spooling\n");
	if (StatusBuffer[3] & 0x08)   printf("				- Drag paper motor on\n");
	if (StatusBuffer[3] & 0x20)   printf("				- LF key pressed\n");
	if (StatusBuffer[3] & 0x40)   printf("				- On/off key pressed\n");
	
	// Recoverable error status:
	if (StatusBuffer[4] & 0x01)   printf("				- Head temperature error\n");
	if (StatusBuffer[4] & 0x08)   printf("				- Power supply voltage error\n");
	if (StatusBuffer[4] & 0x20)   printf("				- Not ack command error\n");
	
	// (Un)recoverable error status:
	if (StatusBuffer[5] & 0x01)   printf("				- Cutter error\n");
	if (StatusBuffer[5] & 0x04)   printf("				- RAM error\n");
	if (StatusBuffer[5] & 0x0C)   printf("				- EEPROM error\n");
	if (StatusBuffer[5] & 0x40)   printf("				- Flash error\n");
}

unsigned long openPrinter(int *iPrinterID, char *strSerialNumberPrinter, int *bOffLineStatus)
{
	unsigned long dwResult = 0;
	// printer :  		       PrinterId   vid      pid    serial number
	dwResult = openPrinterDevice(iPrinterID, lPrinterVid, lPrinterPid, strSerialNumberPrinter);
	if (dwResult != SUCCESS)
	{
		*bOffLineStatus = 1;
		printf("%s:		openPrinterDevice error %lu\n", strSerialNumberPrinter, dwResult);
	}
	*bOffLineStatus = 0;
	return dwResult;
}

void GetStatus(int *iPrinterID, char *strSerialNumberPrinter, int *bOffLineStatus)
{
	unsigned long dwRead = 0;
	unsigned char buffer[READ_BUFFER_SIZE];
	bzero(buffer, READ_BUFFER_SIZE);
	unsigned long dwResult = 0;

	if (*bOffLineStatus)
	{
		dwResult = openPrinterDevice(iPrinterID, lPrinterVid, lPrinterPid, strSerialNumberPrinter);
		// check openUSBDevice result if is not ok exit and show error number see the file ComErrors.h
		if (dwResult != SUCCESS)
		{
			printf("%s:		Printer is OFF LINE (error %lu)\n", strSerialNumberPrinter, dwResult);
			*bOffLineStatus = 1;
		}
	else
		*bOffLineStatus = 0;
	}
	if (!*bOffLineStatus)
	{
		dwResult = GetPrinterDeviceStatus(*iPrinterID, buffer, READ_BUFFER_SIZE, &dwRead);
		if (dwResult != SUCCESS)
		{
			printf("%s:		Printer is OFF LINE (error %lu)\n", strSerialNumberPrinter,dwResult);
			if (!*bOffLineStatus)
				closePrinterDevice(*iPrinterID);
			*bOffLineStatus = 1;
		}
	}
		// show hex printer response
		if (!*bOffLineStatus)
		{
			
			printf("%s:		Printer Response: %x|%x|%x|%x|%x|%x\n",strSerialNumberPrinter,
					buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);
			
			// process the printer status
			checkStatus(buffer);
		}
}
/*
    main program
*/

int main()
{
	int iLoop = 0;
	unsigned long dwResult = 0;
        char* str_ver;
	//Get CeSmLm.so Version
  	str_ver = GetLibVers();
	printf("CeSmLm.so version is : %s\n", str_ver);
	// OPEN PRINTER 1
	openPrinter(&iPrinterID1, strSerialNumberPrinterID1, &m_bOffLineStatusPrinterID1);
	// OPEN PRINTER 2
	openPrinter(&iPrinterID2, strSerialNumberPrinterID2, &m_bOffLineStatusPrinterID2);

	// read 600 times the printer status
	for (iLoop = 0; iLoop < 100; iLoop++)
	{
		GetStatus(&iPrinterID1, strSerialNumberPrinterID1, &m_bOffLineStatusPrinterID1);

		GetStatus(&iPrinterID2, strSerialNumberPrinterID2, &m_bOffLineStatusPrinterID2);
		// sleep 1 sec
		sleep(1);
	}

	// CLOSE PRINTER 1
	dwResult = closePrinterDevice(iPrinterID1);
	if (dwResult != SUCCESS)
	{
		printf("%s:		closePrinterDevice error %lu\n", strSerialNumberPrinterID1,dwResult);
	}
	dwResult = closePrinterDevice(iPrinterID2);
	// CLOSE PRINTER 2
	if (dwResult != SUCCESS)
	{
		printf("%s:		closePrinterDevice error %lu\n", strSerialNumberPrinterID2, dwResult);
	}
	
	return 0;
}

