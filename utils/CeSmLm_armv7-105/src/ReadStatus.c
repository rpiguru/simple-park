/***************************************************************************
                          ReadStatus.c  -  description
                             -------------------
    copyright            : (C) 2009 by Custom Engineering S.p.A
    email                : support @custom.it
    
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <time.h> 
#include <stdio.h>
#include <assert.h>
#include <sys/time.h>

// include libCeSmLm headers
#include "include/CeSmLm.h"
#include "include/ComErrors.h"

// if you install CeSmLm.h and ComErrors.h in the directory /usr/include change include in:
// include <CeSmLm.h>
// include <ComErrors.h>

// define read buffer max size
#define READ_BUFFER_SIZE      2024 // 2k

/*

  checkStatus:  
  this function proces the printer status buffer and show error or interestig status

*/

void checkStatus(unsigned char *StatusBuffer)
{
  // test if the printer response is ok
  if (!(StatusBuffer[0] & 0x10))   printf("          - error full status\n");
  if (!(StatusBuffer[1] & 0x0F))   printf("          - error full status\n");

  // paper status:
  if (StatusBuffer[2] & 0x01)   printf("          - No paper\n");
  if (StatusBuffer[2] & 0x04)   printf("          - Near paper end\n");
  if (StatusBuffer[2] & 0x20)   printf("          - Ticket Out\n");
  
  // user status:
  if (StatusBuffer[3] & 0x03)   printf("          - Cover opened\n");
  if (StatusBuffer[3] & 0x04)   printf("          - Spooling\n");
  if (StatusBuffer[3] & 0x08)   printf("          - Drag paper motor on\n");
  if (StatusBuffer[3] & 0x20)   printf("          - LF key pressed\n");
  if (StatusBuffer[3] & 0x40)   printf("          - On/off key pressed\n");

  // Recoverable error status:
  if (StatusBuffer[4] & 0x01)   printf("          - Head temperature error\n");
  if (StatusBuffer[4] & 0x08)   printf("          - Power supply voltage error\n");
  if (StatusBuffer[4] & 0x20)   printf("          - Not ack command error\n");

  // (Un)recoverable error status:
  if (StatusBuffer[5] & 0x01)   printf("          - Cutter error\n");
  if (StatusBuffer[5] & 0x04)   printf("          - RAM error\n");
  if (StatusBuffer[5] & 0x0C)   printf("          - EEPROM error\n");
  if (StatusBuffer[5] & 0x40)   printf("          - Flash error\n");  
}

/*
    main program
*/

int main()
{
  int iLoop = 0;
  struct tm* Localtime;
  int m_bOffLineStatus = 0;  
  unsigned long dwRead = 0;
  struct timeval SecMicrosec;  
  unsigned long dwResult = 0;
  unsigned char buffer[READ_BUFFER_SIZE];
  char* str_ver;

  str_ver = GetLibVers();
  printf("CeSmLm.so version is : %s\n", str_ver);

  // initialize th buffer
  bzero(buffer, READ_BUFFER_SIZE);

  // init and open the usb status device
  // printer :              vid      pid    serial number
  dwResult = openUSBDevice(0x0DD4, 0x0205, NULL);

  // check openUSBDevice result if is not ok exit and show error number see the file ComErrors.h
  if (dwResult != SUCCESS)
  {
    printf("openUSBDevice error %lu\n", dwResult);
    m_bOffLineStatus = 1;
//    exit(-1);
  }  

  // read 1 minutes the printer status
  for (iLoop = 0; iLoop < 600; iLoop++)
  {
    if (m_bOffLineStatus)
    {
      dwResult = openUSBDevice(0x0DD4, 0x0205,NULL);

      // check openUSBDevice result if is not ok exit and show error number see the file ComErrors.h
      if (dwResult != SUCCESS)
      {
        printf("Printer is OFF LINE --> fail function openUSBDevice (error %lu)\n", dwResult);
        m_bOffLineStatus = 1;
      }
      else
        m_bOffLineStatus = 0;
    }
    if (!m_bOffLineStatus)
    {
      dwResult = GetPrinterStatus(buffer, READ_BUFFER_SIZE, &dwRead);
      if (dwResult != SUCCESS)
      {
        printf("Printer is  OFF LINE --> fail function GetPrinterStatus (error %lu)\n", dwResult);
        if (!m_bOffLineStatus)
          closeUSBDevice();
        m_bOffLineStatus = 1;
      }
    }
    // show hex printer response
    if (!m_bOffLineStatus)
    {    
      gettimeofday(&SecMicrosec, NULL);
      Localtime = localtime(&(SecMicrosec.tv_sec));

      printf("%d-%02d-%02d %02d:%02d:%02d.%03ld  Printer Response: %x|%x|%x|%x|%x|%x\n",
                  Localtime->tm_year+1900, Localtime->tm_mon+1,
                  Localtime->tm_mday, Localtime->tm_hour, Localtime->tm_min,
                  Localtime->tm_sec, (SecMicrosec.tv_usec)/1000,
                  buffer[0], buffer[1], buffer[2], buffer[3], buffer[4], buffer[5]);

      // process the printer status
      checkStatus(buffer);
    }
    // sleep 1 sec
    sleep(1);
  }

	dwResult = closeUSBDevice();

  // check closeUSBDevice result if is not ok exit and show error number see the file ComErrors.h
  if (dwResult != SUCCESS)
  {
    printf("closeUSBDevice error %lu\n", dwResult);
    closeUSBDevice();
    exit(-1);
  }
  
  return 0;
}


