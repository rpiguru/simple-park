#!/usr/bin/env bash
g++ -I. -W -Wall src/ReadStatus.c -o bin/ReadStatus -Llib/ -Wl,-rpath -Wl,../lib/ -lCeSmLm -L/usr/local/lib/ -Wl,-rpath -Wl,/usr/local/lib/
g++ -I. -W -Wall src/ReadStatusSimple.c -o bin/ReadStatusSimple -Llib/ -Wl,-rpath -Wl,../lib/ -lCeSmLm -L/usr/local/lib/ -Wl,-rpath -Wl,/usr/local/lib/
g++ -W -Wall src/ReadStatus2Printers.c -o bin/ReadStatus2Printers -Llib/ -Wl,-rpath -Wl,../lib/ -lCeSmLm -L/usr/local/lib/ -Wl,-rpath -Wl,/usr/local/lib/
