# Custom Engineering S.p.A Status Monitor rel 1.04


This is the  Custom Engineering S.p.A  Status Monitor  package, containing:

    CeSmL
    |
    |-> ReadmeFirst.txt
    |
    |-> build_examples.sh
    |
    |-/lib
    |   |-> libCeSmLm.so
    |
    |-/include
    |   |-> CeSmLm.h 
    |   |-> ComErrors.h 
    |
    |-/src
    |   |-> ReadStatus.c
    |   |-> ReadStatus2Printers
    |
    |
    |-/bin
    |   |-> ReadStatus
    |   |-> ReadStatus2Printers


## Package description:

### 1. lib/ 

libCeSmLm.so : iste the custom enginnerin Shared Objet and provide the function to manage the printer status

### 2. include/ 

CeSmLm.h : CeSmLm headers
ComErrors.h : CeSmLm map error

### 3. src/ 

ReadStatus.c is a simple example to read the printer status, use the libCeSmLm functions
ReadStatus2Printers.c is a simple example to read the printer status, use the libCeSmLm functions from more than 1 printer

### 4. bin/

ReadStatus pre-compiled example
ReadStatus2Printers pre-compiled example

%%% 5. udev/
 	
99-customXXX-prn.rules : udev rules that has to be copied to /etc/udev/rules.d/ with the correct PID of the needed printer

### 6. This ReadmeFirst file

### 7. build_example.sh sctipr to build example


## libCeSmLm functions

 - DWORD openUSBDevice(long,long,const char*);
   This function initialized and opens the USB printer status connection , with the specified parameters.
   Parameters:
   - Vendor ID (int)
   - Product ID (int)
   - Serial Number (constant string)
   Usage example: InitDeviceIf0(0xdd4, 0x01A8, ,TG2480H Num.: 0");
    Note:
    - 1st parameter = -1 : all Vendor IDs will be accepted (be careful!!!) ***
    - 2nd parameter = -1 : all Product IDs will be accepted
    - 3rd parameter = NULL or 'empty string' : all Serial Numbers will be accepted
    
   Return values:

   - SUCCESS      
   - ERR_NO_INIT
   - ERR_OPEN_DEVICE
   - ERR_ALREADY_OPENED
   - ERR_PARAMINIT_NOT_VALID

 - DWORD GetPrinterStatus(unsigned char* bufferRecv,const DWORD dwSize,DWORD* dwRead);
   This function the printer status 
   Parameters:
   - buffer to store data in
   - size of the provided buffer
   - return parameter, where the function will store actually read data size

   Return values:
   - ERR_NOT_OPENED
   - ERR_IOCTL_SEND
   - ERR_IOCTL_PIPE
   - ERR_IOCTL_OVERFLOW
   - ERR_IOCTL_LOW_LEVEL
   - ERR_IOCTL_GENERAL
   - ERR_IOCTL_TIMEOUT
   - ERR_READ_BUFFER_FULL
   - ERR_READING_USB
   - SUCCESs


 - DWORD closeUSBDevice();
   This function closes the printer USB status device.
   
   Return values:
   - ERR_NOT_OPENED
   - ERR_CLOSE
   - SUCCESS

 - DWORD openPrinterDevice(int *iPrinterID, long,long,const char*);
   This function initialized and opens the USB printer status connection , with the specified parameters.
   Parameters:
   - Printer Vector
   - Vendor ID (int)
   - Product ID (int)
   - Serial Number (constant string)
   Usage example: InitDeviceIf0(0xdd4, 0x01A8, ,TG2480H Num.: 0");
    Note:
    - 2nd parameter = -1 : all Vendor IDs will be accepted (be careful!!!) ***
    - 3rd parameter = -1 : all Product IDs will be accepted
    - 4th parameter = NULL or 'empty string' : all Serial Numbers will be accepted

   Return values:

   - SUCCESS
   - ERR_NO_INIT
   - ERR_OPEN_DEVICE
   - ERR_ALREADY_OPENED
   - ERR_PARAMINIT_NOT_VALID

 - DWORD GetPrinterDeviceStatus(int iPrinterID, unsigned char* bufferRecv,const DWORD dwSize,DWORD* dwRead);
   This function the printer status
   Parameters:
   - Printer Vector
   - buffer to store data in
   - size of the provided buffer
   - return parameter, where the function will store actually read data size

   Return values:
   - ERR_NOT_OPENED
   - ERR_IOCTL_SEND
   - ERR_IOCTL_PIPE
   - ERR_IOCTL_OVERFLOW
   - ERR_IOCTL_LOW_LEVEL
   - ERR_IOCTL_GENERAL
   - ERR_IOCTL_TIMEOUT
   - ERR_READ_BUFFER_FULL
   - ERR_READING_USB
   - SUCCESs

 - DWORD closePrinterDevice(int iPrinterID);
   This function closes the printer USB status device.
   Parameters:
   - Printer Vector

   Return values:
   - ERR_NOT_OPENED
   - ERR_CLOSE
   - SUCCESS

 - char* GetLibVers();
   This function get the library CeSmLm Version.

