import time
import RPi.GPIO as GPIO
from settings import PIN_ENTRY_ARM, RELAY_DURATION, PIN_EXIT_ARM
from utils.common import logger

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(PIN_ENTRY_ARM, GPIO.OUT)
GPIO.output(PIN_ENTRY_ARM, False)
GPIO.setup(PIN_EXIT_ARM, GPIO.OUT)
GPIO.output(PIN_EXIT_ARM, False)


def raise_arm(gate='entry', duration=RELAY_DURATION):
    """
    Send `Raise Arm` signal to the boom gate controller to raise the arm.
    :param gate: 'entry' or 'exit'
    :param duration:
    :return:
    """
    logger.info('Raising the {} arm for {} seconds'.format(gate, duration))
    try:
        pin = PIN_ENTRY_ARM if gate == 'entry' else PIN_EXIT_ARM
        GPIO.output(pin, True)
        time.sleep(duration)
        GPIO.output(pin, False)
    except Exception as e:
        logger.exception('Failed to raise arm - {}'.format(e))


def move_arm(gate='entry', value=True):
    """
    Constantly give 3V3 to the relay pin or not.
    :param gate:
    :param value:
    :return:
    """
    logger.info('Constantly give {}V to {} gate'.format(3.3 if value else 0, gate))
    try:
        pin = PIN_ENTRY_ARM if gate == 'entry' else PIN_EXIT_ARM
        GPIO.output(pin, value)
    except Exception as e:
        logger.exception('Failed to lock/unlock up arm - {}'.format(e))
