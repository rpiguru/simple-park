# -*- coding: iso8859-15 -*-

"""
    Utility for date & time operations
"""
import glob
import datetime
import subprocess
import pytz

from utils.common import logger, is_rpi


def epoch_to_datetime(epoch):
    return datetime.datetime.fromtimestamp(epoch)


def update_system_datetime(utc_dt):
    str_new_time = utc_dt.strftime('%Y-%m-%d %H:%M:%S')
    if is_rpi():
        p = subprocess.Popen(['date --set="{}"'.format(str_new_time)], shell=True)
        p.communicate()
        p.wait()
        logger.info('Updating system date & time: {}'.format(str_new_time))
    else:
        logger.info('Setting new UTC date & time: {}'.format(str_new_time))


def get_all_timezone_names():
    """
    Get Olson timezone names
    :return: List of Olson timezone names
    """
    # FIXME: If we need additional timezones out of US/Europe, just add here!
    continental_list = ['US', 'Europe']

    zone_list = []
    if is_rpi():
        for cont in continental_list:
            for zone in glob.glob('/usr/share/zoneinfo/{}/*'.format(cont)):
                zone_list.append('{}/{}'.format(*(zone.split('/')[-2:])))
    else:
        zone_list = ['US/Aleutian', 'US/Arizona', 'US/Indiana-Starke', 'US/Mountain', 'US/Pacific', 'US/Samoa',
                     'US/Hawaii', 'US/Central', 'US/Eastern', 'US/Alaska', 'US/East-Indiana', 'US/Michigan',
                     'US/Pacific-New', 'Europe/Podgorica', 'Europe/Oslo', 'Europe/Stockholm', 'Europe/Tiraspol',
                     'Europe/Bucharest', 'Europe/Mariehamn', 'Europe/Nicosia', 'Europe/Paris', 'Europe/Amsterdam',
                     'Europe/Belgrade', 'Europe/Malta', 'Europe/Lisbon', 'Europe/Budapest', 'Europe/Kiev',
                     'Europe/London', 'Europe/Belfast', 'Europe/Monaco', 'Europe/Vatican', 'Europe/Warsaw',
                     'Europe/Gibraltar', 'Europe/Istanbul', 'Europe/Astrakhan', 'Europe/Jersey', 'Europe/Andorra',
                     'Europe/Uzhgorod', 'Europe/Madrid', 'Europe/Moscow', 'Europe/Isle_of_Man', 'Europe/Chisinau',
                     'Europe/Vaduz', 'Europe/San_Marino', 'Europe/Zaporozhye', 'Europe/Vilnius', 'Europe/Simferopol',
                     'Europe/Vienna', 'Europe/Tallinn', 'Europe/Guernsey', 'Europe/Athens', 'Europe/Zurich',
                     'Europe/Helsinki', 'Europe/Ulyanovsk', 'Europe/Zagreb', 'Europe/Bratislava', 'Europe/Saratov',
                     'Europe/Dublin', 'Europe/Sarajevo', 'Europe/Berlin', 'Europe/Skopje', 'Europe/Luxembourg',
                     'Europe/Prague', 'Europe/Riga', 'Europe/Sofia', 'Europe/Ljubljana', 'Europe/Copenhagen',
                     'Europe/Tirane', 'Europe/Rome', 'Europe/Minsk', 'Europe/Kirov', 'Europe/Kaliningrad',
                     'Europe/Samara', 'Europe/Brussels', 'Europe/Volgograd', 'Europe/Busingen']

    # Ignore timezone that is not supported by `pytz` package.
    tz_list = []
    for tz in zone_list:
        try:
            pytz.timezone(tz)
            tz_list.append(tz)
        except pytz.UnknownTimeZoneError:
            pass
    return tz_list


timezone_list = get_all_timezone_names()


if __name__ == '__main__':

    print(get_all_timezone_names())
