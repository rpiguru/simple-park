import os

from kivy.app import App
from kivy.clock import Clock
from kivy.lang import Builder
from kivy.properties import StringProperty, NumericProperty, BooleanProperty, ObjectProperty
from kivy.uix.modalview import ModalView
from kivymd.dialog import MDDialog


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'dialog.kv'))


class InsideTouchMixin(object):

    def __init__(self, **kwargs):
        super(InsideTouchMixin, self).__init__(**kwargs)
        self.bind(on_touch_down=App.get_running_app().on_modal_touched)


class PasswordDialog(InsideTouchMixin, MDDialog):
    pwd = StringProperty('')
    is_error = BooleanProperty(False)

    def __init__(self, **kwargs):
        self.register_event_type('on_success')
        super(PasswordDialog, self).__init__(**kwargs)

    def on_open(self):
        self.clear_error()
        self.ids.txt_pwd.text = ''
        self.ids.txt_pwd.focus = True

    def on_success(self, *args):
        pass

    def on_ok(self):
        if self.ids.txt_pwd.text == self.pwd:
            self.dismiss()
            self.dispatch('on_success')
        else:
            self.is_error = True

    def clear_error(self):
        self.is_error = False

    def on_touch_down(self, touch):
        if self.is_error:
            self.ids.txt_pwd.text = ''
            self.is_error = False
        super(PasswordDialog, self).on_touch_down(touch)


class LoadingDialog(ModalView):
    pass


class YesNoDialog(InsideTouchMixin, MDDialog):

    message = StringProperty('')

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(YesNoDialog, self).__init__(**kwargs)

    def on_yes(self):
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass


class InputDialog(InsideTouchMixin, MDDialog):

    text = StringProperty('')
    hint_text = StringProperty('')
    input_filter = ObjectProperty(None)

    def __init__(self, **kwargs):
        self.register_event_type('on_confirm')
        super(InputDialog, self).__init__(**kwargs)
        Clock.schedule_once(lambda dt: self.enable_focus(), .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self):
        self.ids.input.focus = True


class TextInputDialog(InsideTouchMixin, MDDialog):

    text = StringProperty('')
    max_length = NumericProperty(50)

    def __init__(self, **kwargs):
        super(TextInputDialog, self).__init__(**kwargs)
        self.register_event_type('on_confirm')
        Clock.schedule_once(lambda dt: self.enable_focus(), .3)

    def on_yes(self):
        new_val = self.ids.input.text
        self.dismiss()
        self.dispatch('on_confirm', new_val)

    def on_confirm(self, *args):
        pass

    def enable_focus(self):
        self.ids.input.focus = True


class NumericKeypadPopup(InsideTouchMixin, ModalView):

    background_color = [0, 0, 0, 0]
    title = StringProperty()
    parent_input = ObjectProperty()

    def __init__(self, **kwargs):
        super(NumericKeypadPopup, self).__init__(**kwargs)
        self.register_event_type('on_keyboard')

    def on_keyboard(self, *args):
        pass

    def on_btn(self, btn):
        self.dispatch('on_keyboard', btn.text)

    def on_dismiss(self):
        self.parent_input.focus = False
