# -*- coding: iso8859-15 -*-

import os
import re
from kivy.core.window import Window
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.properties import StringProperty, BooleanProperty, NumericProperty
from kivy.uix.textinput import TextInput
from kivymd.textfields import MDTextField
from utils.common import get_screen_resolution
from widgets.dialog import InputDialog, NumericKeypadPopup

from .base import DefaultInput, LabeledInputBase, AnimatedWidget
from kivy.clock import Clock


Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'input.kv'))


class SPTextInput(TextInput, DefaultInput, AnimatedWidget):

    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    max_length = NumericProperty(100)

    def insert_text(self, substring, from_undo=False):
        super(SPTextInput, self).insert_text(substring, from_undo)
        self.text = self.text.lstrip()[:self.max_length]

    def on_text_validate(self):
        next_widget = self._get_focus_next('focus_next')
        if next_widget is not None:
            next_widget.focus = True

    def _get_value(self):
        return self.text

    def set_value(self, value):
        self.text = value


class LabeledTextInputBase(LabeledInputBase):
    hint_text = StringProperty('')
    password = BooleanProperty(False)


class LabeledTextInput(LabeledTextInputBase):
    pass


class SPDateInput(SPTextInput):
    def mark_as_normal(self):
        pass

    def custom_error_check(self):
        pass

    def mark_as_error(self):
        pass

    hint_text = StringProperty('MM/DD/YYYY')

    def insert_text(self, substring, from_undo=False):
        """
        Format date input
        :param substring:
        :param from_undo:
        :return:
        """
        super(SPDateInput, self).insert_text(substring, from_undo)
        digits = re.findall(r'\d+', self.text)
        digits = ''.join(digits)
        formated_string = ''

        i = 0
        for c in self.hint_text:
            if str.isalnum(c):
                if i == len(digits):
                    break
                formated_string += digits[i]
                i += 1
            else:
                formated_string += c
        self.text = formated_string
        self.cursor = (len(formated_string), 0)

    def _get_value(self):
        data = re.findall(r'\d+', self.text)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])
        return '-'.join(date[::-1]) if date else ''


class LabeledDateInput(LabeledTextInputBase):
    hint_text = StringProperty('MM/DD/YYYY')

    def _get_value(self):  # TODO looks like unreachable code
        data = super(LabeledDateInput, self)._get_value()
        data = re.findall(r'\d+', data)
        # convert date from mm-dd-yyyy to yyyy-mm-dd format
        date = []
        if len(data) > 0:
            date.append(data[0])

        if len(data) > 1:
            date.append(data[1])
            date[0], date[1] = date[1], date[0]

        if len(data) > 2:
            date.append(data[2])

        return '-'.join(date[::-1]) if date else ''


class NumericInput(MDTextField):

    def __init__(self, **kwargs):
        super(NumericInput, self).__init__(**kwargs)

    def on_touch_down(self, touch):
        # super(NumericInput, self).on_touch_down(touch)
        if not self.disabled:
            if self.collide_point(touch.x, touch.y):
                Window.release_all_keyboards()
                self._show_keypad_popup()
            else:
                self.focus = False

    def _show_keypad_popup(self, *args):
        popup = NumericKeypadPopup(parent_input=self, title=self.hint_text)
        widget_x, widget_y = self.to_window(self.x, self.y)
        w, h = get_screen_resolution()
        x = widget_x if widget_x < w - popup.width else widget_x + self.width - popup.width
        y = widget_y - popup.height + 20 if widget_y > popup.height else widget_y + self.height - 20
        popup.pos_hint = {'center_x': (x + popup.width / 2) / w,
                          'center_y': (y + popup.height / 2) / h
                          }
        popup.bind(on_keyboard=self.on_keypad_pressed)
        popup.open()

    def on__keyboard(self, *args):
        if self._keyboard:
            # Move the original docked keyboard to outside of the window.
            self._keyboard.widget.pos = (1000, 0)

    def on_keypad_pressed(self, *args):
        new_val = args[1]
        if new_val == '<':
            self.text = self.text[:-1]
        else:
            self.insert_text(new_val)
        self.focus = True


class SPMDTextField(MDTextField):
    ignore_clear = BooleanProperty(False)
    ignore_validate = BooleanProperty(False)
    use_dlg = BooleanProperty(False)
    dlg_width = NumericProperty(.5)

    def __init__(self, **kwargs):
        super(SPMDTextField, self).__init__(**kwargs)
        Clock.schedule_once(self.bind_dlg)

    def bind_dlg(self, *args):
        if self.use_dlg:
            self.bind(focus=self.show_dlg)

    def show_dlg(self, inst, val):
        if val and not self.disabled:
            Window.release_all_keyboards()
            dlg = InputDialog(text=self.text, hint_text=self.hint_text)
            dlg.size_hint_x = self.dlg_width
            dlg.bind(on_confirm=self.on_confirm)
            dlg.open()

    def on_confirm(self, *args):
        self.text = args[1]
        self.dispatch('on_text_validate')


Factory.register('SPTextInput', cls=SPTextInput)
Factory.register('LabeledTextInput', cls=LabeledTextInput)
Factory.register('SPDateInput', cls=SPDateInput)
Factory.register('LabeledDateInput', cls=LabeledDateInput)
Factory.register('SPMDTextField', cls=SPMDTextField)
Factory.register('NumericInput', cls=NumericInput)
