import os

from kivy.lang import Builder
from kivy.properties import NumericProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout

from kivymd.dialog import MDDialog
from kivymd.snackbar import Snackbar
from widgets.dialog import InsideTouchMixin

Builder.load_file(os.path.join(os.path.dirname(__file__), 'kv', 'permanent_ticket.kv'))


class PermanentTicketItem(BoxLayout):

    code = NumericProperty()
    name = StringProperty()

    def __init__(self, **kwargs):
        super(PermanentTicketItem, self).__init__(**kwargs)
        self.register_event_type('on_edit')
        self.register_event_type('on_remove')

    def on_btn_edit(self):
        self.dispatch('on_edit')

    def on_btn_remove(self):
        self.dispatch('on_remove')

    def on_edit(self):
        pass

    def on_remove(self):
        pass


class PermanentTicketDlg(InsideTouchMixin, MDDialog):
    code = NumericProperty(-1)
    name = StringProperty()

    def __init__(self, **kwargs):
        super(PermanentTicketDlg, self).__init__(**kwargs)
        self.register_event_type('on_confirm')

    def on_open(self):
        if self.code > 0:
            self.ids.code.text = str(self.code)
        self.ids.name.text = self.name

    def get_value(self):
        return {'code': int(self.ids.code.text), 'name': self.ids.name.text}

    def on_btn_yes(self):
        if len(self.ids.code.text) != 4:
            Snackbar(text='Invalid Code Value!', background_color=(.8, 0, .3, .5)).show()
            return
        self.dispatch('on_confirm')

    def on_confirm(self):
        pass
