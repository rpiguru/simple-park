import argparse
from utils.db import add_permanent_ticket


if __name__ == '__main__':

    print('========== Adding permanent code... ==========')

    parser = argparse.ArgumentParser()

    parser.add_argument("-c", "--code", help="Code value to be added.", type=int)
    parser.add_argument("-n", "--name", help="Code Name")

    args = parser.parse_args()

    if args.code and args.name:
        r, msg = add_permanent_ticket(code=args.code, name=args.name)
        if r:
            print('Created a new permanent code, name: {}, code: {}, response - {}'.format(args.name, args.code, msg))
        else:
            print('Failed to add a new permanent code - {}'.format(msg))
    else:
        print('Please set code and name!')
        print('Example: `sudo python add_permanent_code.py -c 1234 -n "My Permanent Code"`')

