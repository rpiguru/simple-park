# Exit gate using 4 Digit Pin Code/Barcode Code

[Project Description](doc/Simple%20Park%20-%20Exit%20Pin%20Code%202.pdf)

## Installation

- Download the latest **Raspbian Stretch Lite** from [here](https://www.raspberrypi.org/downloads/raspbian/) and flash your MicroSD card.

- After booting your RPi, clone this repo to the home directory:

        cd /home/pi
        sudo apt-get install git
        git clone https://gitlab.com/rpiguru/simple-park

## 1. RPi at Entry Gate

### 1.1 Components

- Raspberry Pi 3B+

- Loop Detector (TBD)

- Label Printer
    
    https://jeatech.com.au/Custom-TG2460H-Kiosk-Printer~1901
    
- Boom Gate (TBD)

- Push Button

### 1.2 Installation

    cd /home/pi/simple-park/scripts
    bash setup-entry.sh
    
After reboot, please read https://www.howtogeek.com/169679/how-to-add-a-printer-to-your-raspberry-pi-or-other-linux-computer/ 
and add printer.

## 2. RPi at Reception

### 2.1 Components

- Raspberry Pi 3B+

- Official 7" touchscreen
    
    https://www.raspberrypi.org/products/raspberry-pi-touch-display/
    
### 2.2 Installation
    
    cd /home/pi/simple-park/scripts
    bash setup-reception.sh

## 3. RPi at Exit Column

### 3.1 Components

- Raspberry Pi 3B+

- Barcode Scanner (TBD)

- Boom Gate (TBD)


### 3.2 Installation

    cd /home/pi/simple-park/scripts
    bash setup-exit.sh
