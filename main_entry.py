"""
Python script to manage RPi at Entry Gate
"""
import threading
import time
from paho.mqtt.client import Client
import keyboard
from settings import PIN_LOOP, PIN_BUTTON, MQTT_HOST, MQTT_TOPIC_ENTRY_RAISE, MQTT_TOPIC_ENTRY_LOCK_UP, \
    MQTT_TOPIC_ENTRY_UNLOCK_UP, MASTER_CODE, PIN_LOOP_B, LOOP_B_TIMEOUT
from utils.arm import raise_arm, move_arm
from utils.barcodes import LOWER_CASE, UPPER_CASE
from utils.common import logger
from utils.pylibCeSmLm import ticket_exist
from utils.web import create_code, get_locked_state, get_code_info, switch_permanent_code, remove_code
from utils.printer import print_ticket
import RPi.GPIO as GPIO


mqtt_client = Client()


class SimpleParkEntry(threading.Thread):

    armed = False
    ticket_printed = False
    is_waiting_loop_b = False
    is_waiting_withdrawn = False
    is_busy = threading.Event()

    lock = threading.RLock()

    buf = ""
    code_info = None

    def __init__(self):
        super(SimpleParkEntry, self).__init__()

    def run(self):
        keyboard.on_press(self.on_pressed)
        threading.Thread(target=self._check_loop_pin).start()
        keyboard.wait()

    def _check_loop_pin(self):
        while True:
            pin_state = GPIO.input(PIN_LOOP)
            with self.lock:
                if self.armed != pin_state:
                    if pin_state:
                        logger.info('=== Vehicle is detected!')
                    else:
                        logger.info('=== Vehicle is disappeared!')
                        if self.is_waiting_withdrawn:
                            logger.error('Whoops, you are leaving without taking your ticket! Removing...')
                            threading.Thread(target=remove_code, args=(self.code_info['code'],)).start()
                            # TODO: Retract ticket here
                            print('Retracting ticket...')
                            self._clear_withdrawn_flag()
                        elif self.ticket_printed:
                            threading.Thread(target=self.start_loop_b_timer).start()
                        self.ticket_printed = False

                    self.armed = pin_state
            time.sleep(.5)

    def on_button_pressed(self, channel):
        logger.info('+++ Button is pressed')
        with self.lock:
            if not self.armed:
                logger.warning('Button(GPIO{}) is pressed, but no vehicle is detected!'.format(channel))
                return
            if self.is_waiting_loop_b:
                logger.warning('I am waiting for the LOOP B now!')
                return
            if self.is_waiting_withdrawn:
                logger.warning('I am waiting for the ticket withdrawn!')
                return
            logger.debug("Checking whether the ticket was already printed or not.")
            if self.ticket_printed:
                logger.error('Ticket can be printed one time only.')
                return

        if self.is_busy.isSet():
            logger.warning('Button is pressed, but I am busy now!')
            return

        logger.debug("Getting locked status")
        if get_locked_state('entry'):
            logger.warning('The gate is locked!')
            return

        self.is_busy.set()
        logger.debug('Creating Code')
        self.code_info = create_code(code_type='temporary')
        if self.code_info:
            logger.info('Created a new ticket - {}, printing it now...'.format(self.code_info))
            print_ticket(self.code_info)
            threading.Thread(target=self.start_withdrawn_timer).start()
            self.is_busy.clear()
            self.ticket_printed = True

    def start_withdrawn_timer(self):
        with self.lock:
            self.is_waiting_withdrawn = True

        logger.info('Waiting for the ticket withdrawn...')
        s_time = time.time()
        while not ticket_exist():
            if time.time() - s_time > 3:
                logger.error('Failed to print the ticket! Printed ticket is not existing..')
                threading.Thread(target=remove_code, args=(self.code_info['code'],)).start()
                self._clear_withdrawn_flag()
                return
            time.sleep(.1)

        while ticket_exist():
            with self.lock:
                # Clear from the outside
                if not self.is_waiting_withdrawn:
                    return

        while GPIO.input(PIN_LOOP_B):
            logger.warning('A ticket was withdrawn, but the previous vehicle is still in LOOP B!')
            time.sleep(.5)
        raise_arm()

        self._clear_withdrawn_flag()

    def _clear_withdrawn_flag(self):
        with self.lock:
            self.is_waiting_withdrawn = False

    def start_loop_b_timer(self):
        logger.info('Starting {} sec timer of LOOP B'.format(LOOP_B_TIMEOUT))
        with self.lock:
            self.is_waiting_loop_b = True

        s_time = time.time()
        while True:
            elapsed = time.time() - s_time
            if GPIO.input(PIN_LOOP_B):
                logger.info('Okay, LOOP B is detected in {} sec'.format(elapsed))
                break
            elif elapsed > LOOP_B_TIMEOUT:
                logger.warning("Sigh, LOOP B is not detected, deleting the ticket now...")
                threading.Thread(target=remove_code, args=(self.code_info['code'], )).start()
                break
            time.sleep(.3)

        with self.lock:
            self.is_waiting_loop_b = False

    def on_pressed(self, event):
        """
        Callback when a key is pressed(Actually from the barcode scanner)
        :param event: keyboard.KeyboardEvent instance
        :return:
        """
        code = event.name
        if code == 'enter':
            logger.debug('==== Barcode is scanned! - {}'.format(self.buf))
            with self.lock:
                if self.is_waiting_withdrawn:
                    logger.warning('I am waiting for the ticket withdrawn!')
                    return
                if self.is_waiting_loop_b:
                    logger.warning('I am waiting for the LOOP B now!')
                    return
            self.parse_code(self.buf)
            self.buf = ""
        elif code in LOWER_CASE or code in UPPER_CASE:
            self.buf = self.buf + code
        else:
            logger.error('Unknown code - {}'.format(code))

    def parse_code(self, code):
        logger.info('Barcode is scanned - {}'.format(code))
        if code == MASTER_CODE:
            raise_arm('entry')
            return

        try:
            code = int(code)
        except ValueError:
            logger.error('Invalid code - {}, type: {}'.format(code, type(code)))
            return

        with self.lock:
            if not self.armed:
                logger.error('Barcode is scanned({}), but the LOOP is not detected!'.format(code))
                return

        if get_locked_state('entry'):
            logger.warning('The gate is locked!')
        else:
            r = get_code_info(code)
            if r.status_code == 200:
                info = r.json()['response']
                logger.info('Code info: {}'.format(info))
                if info['type'] == 'permanent':
                    if info.get('src') != 'entry':
                        state, resp = switch_permanent_code(code=code, src='entry')
                        if state and resp.get('response') == 'OK':
                            raise_arm('entry')
                        else:
                            logger.error('Failed to change the status of the permanent code - {}'.format(resp))
                    else:
                        logger.error('Cannot raise arm. You are scanning a permanent code twice!')
                else:
                    logger.error('Only the permanent code can work.')
            else:
                logger.error('Cannot find details of the code! - {}'.format(r.status_code))


def on_mqtt_messaged(*args):
    topic = args[2].topic
    msg = args[2].payload.decode('utf-8')
    logger.info('Received MQTT message, topic: {}, payload: {}'.format(topic, msg))
    if topic == MQTT_TOPIC_ENTRY_RAISE:
        raise_arm(gate='entry')
    elif topic == MQTT_TOPIC_ENTRY_LOCK_UP:
        move_arm(gate='entry', value=True)
    elif topic == MQTT_TOPIC_ENTRY_UNLOCK_UP:
        move_arm(gate='entry', value=False)


def on_mqtt_connected(client, userdata, flags, rc):
    logger.info('MQTT: Connected to the Broker, Host: {}, Port: 1883, UserData: {}, Flags: {}, Result code: {}'.format(
        MQTT_HOST, userdata, flags, rc))
    mqtt_client.subscribe(MQTT_TOPIC_ENTRY_RAISE)
    mqtt_client.subscribe(MQTT_TOPIC_ENTRY_LOCK_UP)
    mqtt_client.subscribe(MQTT_TOPIC_ENTRY_UNLOCK_UP)


if __name__ == '__main__':

    time.sleep(10)

    logger.info('========== Starting SimplePark Entry Program ==========')

    mqtt_client.on_connect = on_mqtt_connected
    mqtt_client.on_message = on_mqtt_messaged
    mqtt_client.connect(MQTT_HOST, 1883)
    mqtt_client.loop_start()

    spe = SimpleParkEntry()

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(PIN_LOOP, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(PIN_LOOP_B, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
    GPIO.setup(PIN_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    GPIO.add_event_detect(PIN_BUTTON, GPIO.FALLING, callback=spe.on_button_pressed, bouncetime=200)

    spe.start()
