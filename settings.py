WEB_HOST = 'localhost'
WEB_PORT = 3000

MQTT_HOST = 'localhost'
MQTT_TOPIC_ENTRY_RAISE = 'simplepark/entry/raise'
MQTT_TOPIC_ENTRY_LOCK_UP = 'simplepark/entry/lock_up'
MQTT_TOPIC_ENTRY_UNLOCK_UP = 'simplepark/entry/unlock_up'
MQTT_TOPIC_EXIT_RAISE = 'simplepark/exit/raise'
MQTT_TOPIC_EXIT_LOCK_UP = 'simplepark/exit/lock_up'
MQTT_TOPIC_EXIT_UNLOCK_UP = 'simplepark/exit/unlock_up'


MONGO_HOST = 'localhost'        # DO NOT change this
MONGO_DB = 'SimplePark'
MONGO_COLLECTION = 'SP'


# Use GPIO21 for the LOOP sensor
PIN_LOOP = 21

# Use GPIO16 for the LOOP-B sensor on Entry
PIN_LOOP_B = 16

# Use GPIO20 for the push button at the entry gate (Pull up)
PIN_BUTTON = 20

# Use GPIO19 for the relay of the RPi at entry gate
PIN_ENTRY_ARM = 19

# Use GPIO26 for the relay of the RPi at exit gate
PIN_EXIT_ARM = 26

# How long(seconds) do we have to drive the relay to raise the arm?
RELAY_DURATION = 2

# Timeout value of the LOOP B
LOOP_B_TIMEOUT = 3

MASTER_CODE = "M1A2S3T4E5R"

# Interval of ticket status checking process
INTERVAL_STATUS = 60


try:
    from local_settings import *
except ImportError:
    pass
